import {NextFunction, Request, Response, Router} from 'express';
import {NO_CONTENT, NOT_ACCEPTABLE, NOT_FOUND, OK} from 'http-status-codes';
import MementoDao from '@daos/Memento/MementoDao';
import {createURIM, datetimeTotimestamp, fullUrl, timestampToDatetime} from '@shared/functions';
import {ACC_DATETIME, MEM_DATETIME, RESOURCE_TYPE} from '@shared/constants';

// Init shared
const router = Router();
const mementoDao = new MementoDao();

// Retrieve a Timemap
router.get('*', async (req: Request, res: Response, next: NextFunction) => {
    if (req.method !== 'GET' || req.query.ext !== 'timemap') {
        next();
        return;
    }
    const page = req.query.page;
    const url = fullUrl(req);
    const range = req.header('accept-memento-range');
    let format = req.header('Accept');
    if (format === '*/*') {
        format = 'application/link-format';
    }
    if (format === 'application/json' || format === 'application/link-format') {
        let timemap;
        if (page && typeof page === 'string') {
            timemap = await mementoDao.getTimemapByPage(url, format, Number.parseInt(page));
        } else if (range) {
            const rangesplit = range.split('/')
            timemap = await mementoDao.getTimemapByRange(url, format, rangesplit[0], rangesplit[1]);
        } else {
            timemap = await mementoDao.getTimemap(url, format);
        }

        res.set('content-type', format)
        res.status(OK).send(timemap);
    } else {
        res.status(NOT_ACCEPTABLE).send();
    }
});

// Directly retrieve a Memento
router.get('*', async (req: Request, res: Response, next: NextFunction) => {
    const factid = req.query.version;
    if (req.method !== 'GET' || req.query.version === undefined || typeof req.query.version !== 'string') {
        next();
        return;
    }
    if (factid && typeof factid === 'string') {
        const memento = await mementoDao.getOne(fullUrl(req), timestampToDatetime(factid));
        const resourceType = await mementoDao.getType(fullUrl(req));
        let resource = hostString(req);
        if (resourceType === RESOURCE_TYPE.MEMENTO_STACK) {
            resource = await mementoDao.getLocation(fullUrl((req)))
        }
        res.append('Link', ['<' + resource + '>; rel=original']);

        if (memento != null) {
            res.status(OK).send(memento.content);
        } else {
            res.status(NOT_FOUND).send();
        }
    }
});

// Retrieve OR or Negotiate Datetime
router.get('*', async (req: Request, res: Response) => {
    let memento;
    const mementoHeader = req.header(ACC_DATETIME);
    const resourceID = fullUrl(req);
    const resourceType = await mementoDao.getType(resourceID);
    if (mementoHeader !== undefined) {
        memento = await mementoDao.getOne(resourceID, mementoHeader);
        if (memento) {
            let contentLocation = createURIM(resourceID, datetimeTotimestamp(memento.revisionID));
            res.header('Content-Location', contentLocation);
            res.header('Vary', 'accept-datetime');
        }
    } else {
        if (resourceType === RESOURCE_TYPE.ORIGINAL_RESOURCE) {
            memento = await mementoDao.getLast(resourceID);
        } else {
            memento = null;
        }
    }
    if (memento !== null){
        res.set(MEM_DATETIME, memento.revisionID);
        if (resourceType === RESOURCE_TYPE.MEMENTO_STACK) {
            const originalLocation = await mementoDao.getLocation(resourceID);
            res.append('Link', ['<' + originalLocation + '>; rel=original']);
        } else {
            res.append('Link', ['<' + resourceID + '>; rel=original']);

        }

        res.status(OK).send(memento.content);
    } else {
        res.status(NOT_FOUND).send();
    }
});

router.post('*', async (req: Request, res: Response) => {
    return create(req, res);
});

router.put('*', async (req: Request, res: Response) => {
    return create(req, res);
});

async function create(req: Request, res: Response) {
    const datetime = req.header('memento-datetime');
    const location = req.header('content-location'); // TODO: Evaluate if this is the best header for that purpose.
    let memento;
    if (datetime && location) { // Create Memento-Resource without OR
        memento = await mementoDao.addToMementoStack(fullUrl(req), req.body.Content, datetimeTotimestamp(datetime), location);
    } else { // Create OR
        memento = await mementoDao.addToOR(fullUrl(req), req.body.Content);
    }
    res.set(MEM_DATETIME, memento.revisionID);
    res.set('Location', createURIM(fullUrl(req), datetimeTotimestamp(memento.revisionID)));
    res.status(OK).send();
}

function hostString(req: Request): string {
    return req.protocol + '://' + req.get('host') + req.path;

}

router.delete('*', async (req: Request, res: Response) => {
    await mementoDao.delete(fullUrl(req));
    res.status(NO_CONTENT).send();
});

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;