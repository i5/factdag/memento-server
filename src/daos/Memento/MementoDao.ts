import Memento, {IMemento} from '@entities/Memento';
import {createURIM, datetimeTotimestamp, timestampToDatetime} from "@shared/functions";
import {RESOURCE_TYPE} from "@shared/constants";

const redis = require('redis');
const { promisify } = require("util");


export interface IMementoDao {
    getOne: (resourceID:string, revisionID: string) => Promise<IMemento | null>;
    getAll: (resourceID:string) => Promise<IMemento[]>;
    addToOR: (resourceID: string, content: string) => Promise<IMemento>;
    delete: (resourceID:string) => Promise<void>;
}

class MementoDao implements IMementoDao {
    client = redis.createClient(process.env.DBPORT || 6379, process.env.DBHOST || "localhost");
    xadd = promisify(this.client.xadd).bind(this.client);
    xrevrange = promisify(this.client.xrevrange).bind(this.client);
    xrange = promisify(this.client.xrange).bind(this.client);
    rpush = promisify(this.client.rpush).bind(this.client);
    lrange = promisify(this.client.lrange).bind(this.client);
    lindex = promisify(this.client.lindex).bind(this.client);
    lpos = promisify(this.client.lpos).bind(this.client);
    set = promisify(this.client.set).bind(this.client);
    get = promisify(this.client.get).bind(this.client);

    /**
     *
     */
    public async getOne(resourceID:string, revisionID: string): Promise<IMemento | null> {
        const result = await this.xrevrange(resourceID, datetimeTotimestamp(revisionID), "-", "COUNT", 1);
        return this.createMementoFromResult(result, resourceID);
    }

    /**
     *
     */
    public async getLast(resourceID: string): Promise<IMemento | null> {
        try {
            const result = await this.xrevrange(resourceID, "+", "-", "COUNT", 1);
            return this.createMementoFromResult(result, resourceID);
        } catch (err) {
            throw err;
        }
    }


    /**
     *
     */
    public async getAll(resourceID: string): Promise<IMemento[]> {
        // TODO
        // client.xrange(req.path, "-", "+", function(err, val) {
        return [] as any;
    }

    public async getTimemapByPage(resourceID: string, format: string, page: number): Promise<any> {
        const pagesize = 100;
        const from = page*pagesize;
        const to = ((page+1)*pagesize)-1;
        const result = await this.lrange("x" + resourceID, from, to);
        let pages = {};
        if (page != 0) {

            pages = {...pages, ...{"prev": await this.getTimeMapPagePointer(resourceID, page, (page-1)*pagesize, (page*pagesize)-1, -1)}};
        }
        const globalLast = await this.lindex("x" + resourceID, -1);
        const currentLast = await this.lindex("x" + resourceID, to);
        if (currentLast < globalLast) {
            pages = {...pages, ...{"next": await this.getTimeMapPagePointer(resourceID, page, (page+1)*pagesize, ((page+2)*pagesize)-1, +1)}};
        }
        return this.serializeTimemap(resourceID, format, result, pages);
    }

    public async getTimeMapPagePointer(resourceID: string, page: number, from: number, to: number, upDown: number) {
        const fromTime = await this.lindex("x" + resourceID, from);
        let toTime = await this.lindex("x" + resourceID, to);
        if (toTime === null) {
            toTime = await this.lindex("x" + resourceID, -1);
        }
        return {
            "from": timestampToDatetime(fromTime),
            "until": timestampToDatetime(toTime),
            "uri": resourceID + "?ext=timemap&page="+(page+upDown)
        }
    }

    public async getTimemapByRange(resourceID: string, format: string, start_range: string, end_range: string): Promise<any> {
        let start = 0;
        let end = -1;
        const start_element = (await this.getOne(resourceID, start_range))?.revisionID; //TODO: Not ideal, becacuse it retrieves 2 complete elements from the database
        const end_element = (await this.getOne(resourceID, end_range))?.revisionID;

        if (start_element && end_element) {
            start =  await this.lpos("x" + resourceID, datetimeTotimestamp(start_element)); // Only work if exists, maybe retrieve edge objects
            end =  await this.lpos("x" + resourceID, datetimeTotimestamp(end_element));
        }
        const result = await this.lrange("x" + resourceID, start, end);
        return this.serializeTimemap(resourceID, format, result)
    }

    public async getTimemap(resourceID: string, format: string): Promise<any> {
        const result = await this.lrange("x" + resourceID, 0, -1);
        return this.serializeTimemap(resourceID, format, result)
    }



    public async serializeTimemap(resourceID: string, format: string, timestamps: any, pages?: Object): Promise<string> {
        let serialization;
        if (format == "application/json") {
            serialization = this.assembleJsonTimeMap(resourceID, timestamps, pages);
        } else {
            serialization = this.assembleLinkFormatTimeMap(resourceID, timestamps);
        }
        return serialization;
    }

    public assembleLinkFormatTimeMap(resourceID: string, timestamps:any) {
        let result = "";
        result += "<" + resourceID + ">;rel=\"timegate original\",\n"; // Link to TimeGate and URI-O
        result += "<" + resourceID + "?ext=timemap>; rel=\"timemap\"; type=\"application/link-format\",\n"; //Link to TimeMap
        timestamps.forEach((value: string) => {
            result += "<" + createURIM(resourceID,value) + ">" +
                "; rel=\"memento\";datetime=\"" + timestampToDatetime(value) + "\",\n";
        })
        return result.substring(0, result.length-2);
    }

    public assembleJsonTimeMap(resourceID: string, timestamps:any, pages?: Object) {
        let mementos: { datetime: string; uri: string; }[] = []
        timestamps.forEach((value: string) => {
            let memento = {
                "datetime" : timestampToDatetime(value),
                "uri" : createURIM(resourceID,value)
            };
            mementos.push(memento)
        });
        let dateObject = {
            "original_uri": resourceID,
            "timemap_uri" : resourceID + "?ext=timemap",
            "mementos": mementos,
        };
        if (pages) {
            dateObject = {...dateObject, ...{"pages": pages}};
        }
        return JSON.stringify(dateObject);
    }

    public async getType(resourceID: string): Promise<RESOURCE_TYPE> {
        return this.get("type:"+ resourceID);
    }

    public async getLocation(resourceID: string): Promise<string> {
        return this.get("location:"+ resourceID);
    }

    private async add(resourceID: string, content: string, id = '*'): Promise<IMemento> {
        const val = await this.xadd(resourceID, id, "content", content);
        await this.rpush("x" + resourceID, val); // Maintain timemap
        return new Memento(resourceID, timestampToDatetime(val), content);
    }


    /**
     *
     */
    public async addToOR(resourceID: string, content: string): Promise<IMemento> {
        const type = await this.getType(resourceID);
        if (!type || type === RESOURCE_TYPE.ORIGINAL_RESOURCE) {
            await this.set("type:" + resourceID, RESOURCE_TYPE.ORIGINAL_RESOURCE);
            return this.add(resourceID, content);
        }
        throw new Error();
    }

    /**
     *
     */
    public async addToMementoStack(resourceID: string, content: string, id: string, orLocation: string): Promise<IMemento> {
        const type = await this.getType(resourceID);
        if (!type || type === RESOURCE_TYPE.MEMENTO_STACK) {
            await this.set("type:" + resourceID, RESOURCE_TYPE.MEMENTO_STACK);
            await this.set("location:" + resourceID, orLocation);
            return this.add(resourceID, content, id);
        }
        throw new Error();
    }

    /**
     *
     */
    // public async update(resourceID: string, content: string): Promise<IMemento> {
    //     return {} as any;
    // }


    /**
     *
     */
    public async delete(resourceID:string): Promise<void> {
        await this.xadd(resourceID, "*", "DELETED", "TOMBSTONE");
        return;
    }

    private createMementoFromResult(result: any, resourceID: string): IMemento | null {
        if (result !== undefined && result.length != 0) {
            if (result[0][1][0] !== "DELETED") {
                const timestamp = result[0][0];
                return new Memento(resourceID, timestampToDatetime(timestamp), result[0][1][1]);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

export default MementoDao;
