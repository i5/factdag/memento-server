export interface IMemento {
    resourceID: string;
    revisionID: string;
    content: string;
}

class Memento implements IMemento {

    constructor(public resourceID: string, public revisionID: string, public content: string) {    }
}

export default Memento;
