import logger from './Logger';
import {Request} from "express";
import Url from "url";

export const pErr = (err: Error) => {
    if (err) {
        logger.error(err);
    }
};

export const getRandomInt = () => {
    return Math.floor(Math.random() * 1_000_000_000_000);
};

export const fullUrl = (req: Request) => {
    return Url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: Url.parse(req.url).pathname,
    });
}

export const timestampToDatetime = (timestamp: string) => {
    const splittime = timestamp.split("-");
    const time = Number(splittime[0]);
    let countstring =  splittime [1];
    const places = Number.parseInt(process.env.SUB_MILLI_PLACES || "3");
    if (places !== -1) {
        const maxval = Math.pow(10, places) - 1;
        if (Number.parseInt(countstring) > maxval ) {
            countstring =  maxval.toString();
        }
        while (countstring.length < places) {
            countstring = "0" + countstring;
        }
    }

    const date = new Date(time);
    let datestring = date.toISOString();
    return (datestring.substring(0, datestring.length-1) + countstring + "Z");
}

export const datetimeTotimestamp = (datetime: string) => {
    const milli = Date.parse(datetime).toString();
    const lastpoint = datetime.lastIndexOf(".");
    const counter = datetime.substring(lastpoint+4, datetime.length-1);
    return milli + "-" + parseInt(counter);
}

export const createURIM = (resourceID: string, timestamp: string) => {
    return  resourceID + "?version=" + timestamp;
}