
export const paramMissingError = 'One or more of the required parameters was missing.';
export const MEM_DATETIME = 'memento-datetime';
export const ACC_DATETIME = 'accept-datetime';

export enum RESOURCE_TYPE {
    MEMENTO_STACK = 'MEMENTO_STACK',
    ORIGINAL_RESOURCE = 'ORIGINAL_RESOURCE',
}

