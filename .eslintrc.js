module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
  },
  'extends': [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 2020,
    'sourceType': 'module',
  },
  'plugins': [
    '@typescript-eslint',
  ],
  'rules': {
    '@typescript-eslint/explicit-function-return-type': "warn",
    'keyword-spacing': 'warn',
    'semi': "warn",
    'brace-style': "warn",
   // 'interface-name-prefix': ["warn", { "prefixWithI": "always", "allowUnderscorePrefix": true }],
  },
};
