import time
from locust import HttpUser, task, constant_pacing
import random
import string
import logging

def get_random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

class MyUser(HttpUser):
    wait_time = constant_pacing(0.05)
    @task
    def standard_update(self):
        content = get_random_string(20)
        postresponse = self.client.post("/timemap_resource", {"Content": content})
        logging.info({"datetime": postresponse.headers['memento-datetime']})