import time
from locust import HttpUser, task, between, SequentialTaskSet, tag, constant_pacing, constant
import logging
import random
import string
from timeit import default_timer as timer


def get_random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


class MyUser(HttpUser):
    wait_time = constant_pacing(0.5)

    @task
    def extended_update(self):
        content = get_random_string(20)
        start = timer()
        postresponse = self.client.post("/resource", {"Content": content})
        end = timer()
        request_time = (postresponse.elapsed.total_seconds()) * 1000
        processing_time = (end - start) * 1000
        logging.info({"type": "extension", "datetime": postresponse.headers['memento-datetime'],
                      "processing-time": processing_time, "request_time": request_time, "content": content})
